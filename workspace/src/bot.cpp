#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "DatosMemCompartida.h"
#include "defines.h"
int main(){
	int fd;
	DatosMemCompartida *ptr;
	struct stat shmobj_st;
	fd = open(BOT_FILE,O_RDWR);

	///////////////////////////como hacer que se cree el archivo si no lo ha creado antes el cliente y no de error
	if (fd==-1){
		printf("error bot: error abriendo el archivo %s\n",BOT_FILE);
		exit(1);
	}
	if(fstat(fd,&shmobj_st)==-1){
		printf("error bot: error con fstat\n");
		exit(1);
	}
	ptr=(DatosMemCompartida *) mmap(NULL, shmobj_st.st_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
	if(ptr==MAP_FAILED){
		printf("error bot: no se pudo abrir mmap\n");
		exit(1);
	}
	close(fd);
	while(1){
	if(ptr->esfera.centro.y<ptr->raqueta1.y1)ptr->accion=-1;
	else if(ptr->esfera.centro.y>ptr->raqueta1.y2)ptr->accion=1;
	else ptr->accion=0;
	usleep(25000);
	};
	unlink(BOT_FILE);
	return 0;
}
