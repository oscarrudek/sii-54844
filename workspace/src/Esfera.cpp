//oscar rudek 54844
// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{	
//la pelota disminuye con el tiempo y la velocidad aumenta
//la velocidad cuando la pelota tiene el radio de 0.5 es de sqrt18 (el modulo de la velocidad en el constructor) y cuando el radio es minimo, la velocidad es 5 veces la inicial
	if(radio>0.3){
		radio-=0.01*t;
	}
	velocidad=velocidad.Unitario()*2*(6-4*radio);
	centro.x=centro.x+velocidad.x*t;
	centro.y=centro.y+velocidad.y*t;
}
