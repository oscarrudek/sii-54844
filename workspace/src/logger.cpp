#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include "defines.h"
int main(){
	if(mkfifo(SERVER_LOGGER_FIFO,0777)==-1){
		if(errno!=EEXIST){
			printf("Error logger: no se pudo crear el fifo %s.\n",SERVER_LOGGER_FIFO);
			return 1;
		}
	}
	int pipe=open(SERVER_LOGGER_FIFO,O_RDONLY);
	if (pipe <0){
		printf("Error logger: no se pudo abrir el fifo %s.\n",SERVER_LOGGER_FIFO);
		return 2;
	}
        char buffer[100];
	int flag=1;
	do{
		int n;
		n=read(pipe,buffer,100);
		buffer[n]='\0';
		char *exit="kill fifo";
		for (int i; i<n;i++){
			if(buffer[i]!=exit[i])break;
			else flag=0;
		}
		if(flag)
			printf("%s",buffer);
	}while(flag);
	close (pipe);
	return 0;
}
